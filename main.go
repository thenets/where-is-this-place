package main

import (
	"encoding/csv"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"strings"
)

func main() {
	// Get arguments
	startServer := true

	// Get tree
	tree := GetTree()

	// Testing binary tree
	if startServer == false {
		fmt.Println("Just testing...")

		// Search path
		result := GetValueByPath(tree, "010")
		fmt.Println(result)

		os.Exit(0)
	}

	// Start web server
	if startServer == true {
		fmt.Print("Servidor iniciado! \\õ/ | Porta: 5000\n\n")

		// Redirect to static HTML files
		http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
			http.Redirect(w, r, "/game", 301)
		})

		// Return static files
		http.Handle("/game/", http.StripPrefix("/game/", http.FileServer(http.Dir("./public"))))

		// Main API data with game decisions
		http.HandleFunc("/api/", func(w http.ResponseWriter, r *http.Request) {
			path := r.URL.Path[4:]

			if path == "/" {
				path = "/root"
			}

			result := GetValueByPath(tree, path[1:])

			if result != "not_found" {
				fmt.Println("Request:", path, ";", result)
				json.NewEncoder(w).Encode(result)
			} else {
				fmt.Println("Request:", path, ";", "ERRO: Não encontrado")
				json.NewEncoder(w).Encode("not_found")
			}

		})

		http.ListenAndServe(":5000", nil)
	}

}

// TreeNode struct of binary tree
type TreeNode struct {
	path  string    // Binary path
	value string    // Main value
	left  *TreeNode // Left  node
	right *TreeNode // right node
}

// GetData return tree with initialized data
func GetData() []TreeNode {
	in := `tree_path,value
root,"Tem cultura oriental?"
0,"Tem uma torre famosa?"
00,"O inglês é a lingua oficial?"
000,"Resposta: Alemanha"
001,"Possui uma família real?"
0010,"Resposta: Estados Unidos da América"
0011,"Resposta: Inglaterra"
01,"É famoso pelos seus perfumes?"
010,"Resposta: Itália"
011,"Resposta: França"
1,"Possui mais de um bilhão de habitantes?"
10,"Seu país é uma potência tecnológica?"
100,"Resposta: Coréia do Norte"
101,"Seu país é um grande produtos de petróleo?"
1010,"Resposta: Japão"
1011,"Resposta: Arábia Saudita"
11,"A vaca é considerada sagrada?"
110,"Resposta: China"
111,"Resposta: Índia"
`
	r := csv.NewReader(strings.NewReader(in))

	var listOfNodes []TreeNode

	for {
		record, err := r.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatal(err)
		}

		listOfNodes = append(listOfNodes, TreeNode{record[0], record[1], nil, nil})
	}

	// fmt.Println(listOfNodes)

	return listOfNodes[1:]
}

// initializeData return tree with initialized data
func initializeData() TreeNode {
	root := TreeNode{"0", "testing", nil, nil}
	root.left = &TreeNode{"00", "jogos", nil, nil}
	root.left.right = &TreeNode{"001", "assassins creed", nil, nil}
	// fmt.Println(root.left.right)

	return root
}

// GetTree return GetData() content as a binary tree
func GetTree() *TreeNode {
	listOfNodes := GetData()

	// lastNode := root

	// Populate tree from "listOfNodes"
	root := &listOfNodes[0]
	for i := 1; i < len(listOfNodes); i++ {
		path := listOfNodes[i].path
		parentPath := string(path[:len(path)-1])
		latestDecision := string(path[len(path)-1:])

		// WORKING EXAMPLE
		// root.left = &TreeNode{"0", "jogos", nil, nil}
		// root.left.left = &TreeNode{"00", "assassins creed", nil, nil}
		// root.left.left.left = &TreeNode{"000", "assassins creed 2", nil, nil}
		// fmt.Println(getNodeByPath(&root, "000"))
		// os.Exit(0)

		// Rename root path
		if len(parentPath) == 0 {
			parentPath = "root"
		}

		// DEBUG
		// fmt.Println("GetTree: path=>", path, "; parentPath=>", parentPath, "; latestDecision=>", latestDecision)

		// Find parent index node
		for j := 0; j < len(listOfNodes); j++ {
			if listOfNodes[j].path == string(parentPath) {
				if latestDecision == "0" {
					listOfNodes[j].left = &listOfNodes[i]
				}
				if latestDecision == "1" {
					listOfNodes[j].right = &listOfNodes[i]
				}

				// DEBUG
				// fmt.Println(listOfNodes[j])
			}
		}
	}

	return root
}

// getNodeByPath return a node for requested path
func getNodeByPath(node *TreeNode, path string) *TreeNode {
	// Check if is nil
	if node == nil {
		panic("Node is NIL!")
	}

	// Just return if root path was requested
	if path == "root" {
		fmt.Println("getNodeByPath: ROOT! node=>", node)
		return node
	}

	// Stop if path is empty
	if len(path) < 1 {
		fmt.Println("getNodeByPath: FIND! node=>", node)
		return node
	}

	// DEBUG
	fmt.Println("getNodeByPath: path=>", path, "; selector=>", string(path[0]), "; currentNode", node)

	// Search for node with TRUE next
	if string(path[0]) == "1" {
		return getNodeByPath(node.right, path[1:])
	}

	// Search for node with FALSE next
	if string(path[0]) == "0" {
		return getNodeByPath(node.left, path[1:])
	}

	fmt.Println("NOTHING!", node)

	return node
}

// GetValueByPath return a node for requested path
func GetValueByPath(tree *TreeNode, path string) string {
	node := *getNodeByPath(tree, path)
	return node.value
}

// SearchPath return the value of 'path' node
// if not found, return string "not_found"
func SearchPath(path string) string {
	listOfNodes := GetData()

	for i := 0; i < len(listOfNodes); i++ {
		// fmt.Println(listOfNodes[i].path)

		if listOfNodes[i].path == path {
			return listOfNodes[i].value
		}
	}

	return "not_found"
}
