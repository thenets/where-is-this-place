NAME = registry.gitlab.com/thenets/where-is-this-place
TAG = latest
SHELL = /bin/sh

build: pre-build docker-build post-build

pre-build:

post-build:

docker-build:
	docker build -t $(NAME):$(TAG) --rm .

shell:
	docker run -it --rm -p 8888:80 --entrypoint="" $(NAME):$(TAG) $(SHELL)

build-shell: build shell

build-test: build test

test:
	docker run -it --rm --name debug -p 8888:80 $(NAME):$(TAG)


binaries:
	go get github.com/inconshreveable/mousetrap
	set GOARCH=386
	set GOOS=windows
	go build -o build/windows/where-is-this-place.exe main.go
	set GOOS=linux 
	go build -o build/linux/where-is-this-place main.go
	set GOOS=darwin 
	go build -o build/macosx/where-is-this-place main.go
