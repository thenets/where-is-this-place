FROM golang:1.9-alpine3.7

RUN mkdir -p /app/public

ADD public/ /app/public/

WORKDIR /app

RUN apk add --no-cache wget && \
    wget https://www.dropbox.com/s/wzkhk5h32yzspoi/where-is-this-place?dl=1 -O where-is-this-place && \
    chmod +x where-is-this-place

ENTRYPOINT [ "./where-is-this-place", "server" ]

EXPOSE 5000
